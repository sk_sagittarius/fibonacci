﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Fibonacci
{
    class Program
    {
        static int Fib(int n)
        {
            return n > 1 ? Fib(n - 1) + Fib(n - 2) : n;
        }
        static void Main(string[] args)
        {
            string[] arr;
            using (FileStream stream = File.OpenRead("input.txt")) //числа в исходном документе должны быть без запятых
            {
                byte[] bytesData = new byte[stream.Length];
                stream.Read(bytesData, 0, bytesData.Length);

                string result = Encoding.UTF8.GetString(bytesData);
                Console.WriteLine("Result " + result);
                arr = result.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            }
            Console.Write("New result ");
            for (int i = arr.Length; i < ((arr.Length) + (arr.Length)); i++)
            {
                Console.Write(Fib(i) + " ");
            }
            Console.ReadLine();
        }
    }
}
